<?php

use App\Http\Controllers\MemberController;
use App\Http\Controllers\SalesController;
use App\Http\Controllers\TransactionController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/member/login', [MemberController::class, 'login']);
Route::post('/sale/login', [SalesController::class, 'login']);
Route::get('/member/qrcode/{member}', [MemberController::class, 'qrCode']);
Route::get('/some_url',function () {
    return response()->json([
        'message' => 'You must logged in !!!'
    ]);
})->name('logged');


Route::group(['middleware' => ['auth:sanctum', 'type.member']], function () {
    Route::get('/member/transaction', [TransactionController::class, 'getTransactionByUser']);
    Route::post('/member/changePwd', [MemberController::class, 'initialiserPwd']);
    Route::post('/logout', [MemberController::class, 'logout']);
});

Route::group(['middleware' => ['auth:sanctum', 'type.sale']], function () {

    Route::post('/sale/transaction', [TransactionController::class, 'makeTransaction']);
    Route::post('/logout', [MemberController::class, 'logout']);

    Route::get('/sale', function (Request $request) {
        return $request->user();
    });
});
