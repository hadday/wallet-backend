<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;


class Member extends Authenticatable
{
    protected $fillable = ["name", "email", "tel", "password", "pwd_init"];
    protected $hidden = [
        'password',
    ];
    use HasApiTokens, HasFactory, Notifiable;

    public function sales()
    {
        return $this->belongsToMany(Sale::class)
            ->withPivot('montant', 'type_transaction')
            ->withTimestamps();
    }
}
