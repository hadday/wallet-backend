<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;

class Sale extends Authenticatable
{
    protected $fillable = ["name", "email", "tel", "password"];
    protected $hidden = [
        'password',
    ];
    use HasApiTokens, HasFactory, Notifiable;

    public function members()
    {
        return $this->belongsToMany(Member::class)
            ->withPivot('montant', 'type_transaction')
            ->withTimestamps();
    }
}
