<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MemberResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            "id"=>$this->id,
            "name"=>$this->name,
            "email"=>$this->email,
            "tel"=>$this->tel,
            "password"=>$this->password,
            "pwd_init"=>$this->pwd_init,
            "sales"=>$this->sales,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
