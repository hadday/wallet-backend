<?php

namespace App\Http\Controllers;

use App\Models\Member;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class MemberController extends BaseController
{

    /**
     * @OA\Post(
     *      path="/api/member/login",
     *      operationId="login",
     *      description="Authentication with Phone & password",
     *      tags={"Member"},
     *   @OA\RequestBody(
     *       @OA\JsonContent(),
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *               type="object",
     *               required={"tel", "password"},
     *               @OA\Property(property="tel",type="string"),
     *               @OA\Property(property="password", type="password"),
     *           )
     *       )
     *   ),
     *     @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *  )
     */
    public function login(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'tel' => 'required|string',
            'password' => 'required|string|min:6'
        ]);

        if ($validator->fails()) {
            return $this->handleError($validator->errors(), "Data Required");
        }

        if (!Auth::attempt(['tel' => $request->tel, 'password' => $request->password])) {
            return $this->handleError('Credentials not match', 401);
        }

        return $this->handleResponse(
            ["token" => auth()->user()->createToken('member', ['role:member'])->plainTextToken],
            "User is logged"
        );
    }

    /**
     * @OA\Get(
     *      path="/api/member/qrcode/{id}",
     *      operationId="qrCode",
     *      description="show QrCode  user",
     *      tags={"Member"},
     *      security={{"Bearer":{}}},
     * @OA\Parameter(
     *      name="id",
     *         in="path",
     *         description="",
     *         required=true,
     *      ),
     *     @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *  )
     */
    public function qrCode(Request $request, $id)
    {
        $member  = Member::find($id);

        if (!$member) {
            return $this->handleError("Member not found");
        }

        $image = \QrCode::size(300)
            ->style('round')
            ->eye('circle')
            ->encoding('ASCII')
            ->generate(json_encode([
                "tel" => $member->tel,
                "name" => $member->name
            ]));
        return $image;
    }


    /**
     * @OA\Post(
     *      path="/api/member/changePwd",
     *      operationId="initialiserPwd",
     *      description="Change password when member log for the first time",
     *      tags={"Member"},
     *      security={{"Bearer":{}}},
     *   @OA\RequestBody(
     *       @OA\JsonContent(),
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *               type="object",
     *               required={"current_password"},
     *               @OA\Property(property="current_password", type="password"),
     *           )
     *       )
     *   ),
     *     @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *  )
     */
    public function initialiserPwd(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'current_password' => 'required',
            // 'password' => 'required|same:confirm_password|min:6',
            // 'confirm_password' => 'required',
        ]);

        $member  = Auth::user();
        // $userPassword = $user->password;

        if ($validator->fails()) {
            return $this->handleError($validator->errors(), "Data Required");
        }

        $current_password = $request->current_password;


        if ($member->pwd_init) {
            return $this->handleError("password already changed");
        }

        $member->password = Hash::make($current_password);
        $member->pwd_init = true;
        $member->save();

        return $this->handleResponse(
            $member,
            "Password changed"
        );
    }



    /**
     * @OA\Post(
     *      path="/api/logout",
     *      operationId="logout",
     *      description="Logout",
     *      tags={"Member, Sales"},
     *      security={{"Bearer":{}}},
     *     @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     *      @OA\Response(
     *          response=400,
     *          description="Bad Request"
     *      ),
     *       @OA\Response(
     *           response=404,
     *           description="not found"
     *       ),
     *  )
     */
    public function logout(Request $request)
    {
        auth()->user()->tokens()->delete();

        return [
            'message' => 'Logged out'
        ];
    }
}
