<?php

namespace App\Http\Controllers;

use App\Http\Resources\MemberResource;
use App\Models\Member;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;


class TransactionController extends BaseController
{


    /**
     * @OA\Get(
     *      path="/api/member/transaction",
     *      operationId="getTransactionByUser",
     *      description="get transaction of  user authenticated",
     *      tags={"Member"},
     *      security={{"Bearer":{}}},
     *     @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *  )
     */
    public function getTransactionByUser()
    {
        $user = Auth::user();

        $transactions = DB::table('member_sale')
            ->where('member_id', '=', $user->id)
            ->get();

        return $this->handleResponse(
            $transactions,
            "get All Transactions"
        );
    }


    /**
     * @OA\Post(
     *      path="/api/sale/transaction",
     *      operationId="makeTransaction",
     *      description="sale submit the transaction of each user",
     *      tags={"Sales"},
     *   @OA\RequestBody(
     *       @OA\JsonContent(),
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *               type="object",
     *               required={"tel", "montant","type_transaction"},
     *               @OA\Property(property="tel",type="string"),
     *               @OA\Property(property="montant",type="number"),
     *               @OA\Property(property="type_transaction", type="string"),
     *           )
     *       )
     *   ),
     *     @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *  )
     */
    public function makeTransaction(Request $request)
    {

        $validator = Validator::make($request->all(), [
            "tel" => "required",
            "montant" => "required",
            "type_transaction" => "required",
        ]);


        if ($validator->fails()) {
            return $this->handleError($validator->errors(), "Data Required");
        }

        $tel = $request->tel;

        $member = Member::where("tel", "=", $tel)->firstOrFail();
        $sale = Auth::guard('admin')->user();


        if (!$member) {
            return $this->handleError("User not found, try another phone number");
        }

        $member->sales()
            ->attach(
                $sale->id,
                [
                    "montant" => $request->montant,
                    "type_transaction" => $request->type_transaction,
                ]
            );

        return $this->handleResponse(
            new MemberResource($member),
            "Transaction succed"
        );
    }
}
