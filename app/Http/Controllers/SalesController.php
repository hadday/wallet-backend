<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class SalesController extends BaseController
{

        /**
     * @OA\Post(
     *      path="/api/sale/login",
     *      operationId="login",
     *      description="Authentication with Phone & password",
     *      tags={"Sales"},
     *   @OA\RequestBody(
     *       @OA\JsonContent(),
     *       @OA\MediaType(
     *           mediaType="multipart/form-data",
     *           @OA\Schema(
     *               type="object",
     *               required={"tel", "password"},
     *               @OA\Property(property="tel",type="string"),
     *               @OA\Property(property="password", type="password"),
     *           )
     *       )
     *   ),
     *     @OA\Response(
     *          response=200,
     *          description="Successful operation",
     *          @OA\MediaType(
     *           mediaType="application/json",
     *      )
     *      ),
     *      @OA\Response(
     *          response=401,
     *          description="Unauthenticated",
     *      ),
     *      @OA\Response(
     *          response=403,
     *          description="Forbidden"
     *      ),
     * @OA\Response(
     *      response=400,
     *      description="Bad Request"
     *   ),
     * @OA\Response(
     *      response=404,
     *      description="not found"
     *   ),
     *  )
     */
    public function login(Request $request){

        $validator = Validator::make($request->all(), [
            'tel' => 'required|string',
            'password' => 'required|string|min:6'
        ]);

        if ($validator->fails()) {
            return $this->handleError($validator->errors(), "Data Required");
        }

        if (!Auth::guard('admin')->attempt(['tel' => $request->tel, 'password' => $request->password])) {
            return $this->handleError('Credentials not match', 401);
        }

        return $this->handleResponse(
            [auth()->guard('admin')->user()->createToken('sale', ['role:sale'])->plainTextToken,Auth::guard('admin')->user()],
            "User is logged"
        );
    }
}
