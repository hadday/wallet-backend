<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{

    /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Wallet App"
     * ),
     *  @OA\SecurityScheme(
     *      securityScheme="Bearer",
     *      in="header",
     *      name="Bearer",
     *      type="http",
     *      scheme="Bearer",
     *      bearerFormat="JWT",
     * ),
     */


    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
