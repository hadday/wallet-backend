<?php

namespace Database\Seeders;

use App\Models\Sale;
use Illuminate\Database\Seeder;

class SalesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Sale::create([
            "name"=>'Admin',
            "email"=>'admin@gmail.com',
            "tel"=>'09876',
            "password"=>bcrypt('123456')
        ]);
    }
}
