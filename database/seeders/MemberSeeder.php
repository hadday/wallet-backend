<?php

namespace Database\Seeders;

use App\Models\Member;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class MemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Member::create([
            "name"=>'othmane',
            "email"=>'oth@gmail.com',
            "tel"=>'123456',
            "password"=>Hash::make('123456'),
            "pwd_init"=>false
        ]);
    }
}
